#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"

void crear_libro(void *ref, size_t tamano){
	Libro *buf=(Libro *)ref;
	buf->autor=(char*)calloc(50,sizeof(char));
	buf->nombre=(char*)calloc(50,sizeof(char));
	buf->precio=0.0f;
	buf->raiting=0;	
}


void eliminar_libro(void *ref, size_t tamano){
	Libro *buf=(Libro *)ref;
	free(buf->autor);
	free(buf->nombre);
	buf->precio = -1;
	buf->raiting = -1;
}
