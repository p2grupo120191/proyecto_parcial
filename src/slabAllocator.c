#include "slaballoc.h"
#include "objetos.h"

SlabAlloc *crear_cache(char *nombre, size_t tamano_objeto, constructor_fn constructor, destructor_fn destructor){

	SlabAlloc *allocator = (SlabAlloc*)calloc(TAMANO_CACHE,sizeof(SlabAlloc));
	allocator->nombre = nombre;
	allocator->tamano_cache = TAMANO_CACHE;
	allocator->tamano_objeto = tamano_objeto;
	allocator->cantidad_en_uso = 0;
	allocator->constructor = constructor;
	allocator->destructor = destructor;
	allocator->mem_ptr = calloc(TAMANO_CACHE,tamano_objeto);
	allocator->slab_ptr = (Slab*)calloc(TAMANO_CACHE,sizeof(Slab));
	int i =0;
	Slab *ptrslab = allocator->slab_ptr;
	for(i = 0;i < TAMANO_CACHE; i++){
		ptrslab[i].status = 0;
		ptrslab[i].ptr_data = (allocator->mem_ptr)+(i*tamano_objeto);
		ptrslab[i].ptr_data_old = NULL;
		
	}
	int x = 0;
	void *ptr = (allocator->mem_ptr);
	for(x = 0; x < TAMANO_CACHE; x++){
		constructor(ptr+(x*tamano_objeto),allocator->tamano_objeto); 
	}  
	return allocator;
}
void *obtener_cache(SlabAlloc *alloc, int crecer){
	if(alloc->cantidad_en_uso == alloc->tamano_cache && crecer == 1){
		int viejo = alloc->tamano_cache;
		Slab *slabviejo = alloc->slab_ptr;
		void *dataviejo = alloc->mem_ptr;
		alloc->tamano_cache = alloc->tamano_cache*2;	
		alloc->slab_ptr = realloc(alloc->slab_ptr,alloc->tamano_cache);
		alloc->mem_ptr = realloc(alloc->mem_ptr,alloc->tamano_cache);
		int i;
		Slab *ptrslab = alloc->slab_ptr;
		for(i = 0; i<alloc->tamano_cache;i++){
			ptrslab[i].ptr_data = (alloc->mem_ptr)+(i*alloc->tamano_objeto);
			if(i < viejo){
				ptrslab[i].ptr_data_old = dataviejo;
				ptrslab[i].status = slabviejo[i].status;
			}else{
				ptrslab[i].ptr_data_old = NULL;
				ptrslab[i].status = 0;
			}
		}
		return ptrslab[(alloc->tamano_cache)+1].ptr_data;
	}
	int i = 0;
	Slab *ptrslab = alloc->slab_ptr;
	for(i = 0; i < alloc->tamano_cache;i++){
		if(ptrslab[i].status == 0){
			alloc->cantidad_en_uso += 1;
			ptrslab[i].status = 1;
			return ptrslab[i].ptr_data;
		}else{
			return NULL;
		}
	}
	return NULL;
}

void devolver_cache(SlabAlloc *alloc, void *obj){
	for(int i=0;i<alloc->tamano_cache;i++){
		if(alloc->slab_ptr[i].ptr_data==obj){
			alloc->slab_ptr[i].status=0;	
		}
	}
}
//Destruir todos los objetos asociados a slabAlloc y despues liberar los punteros
void destruir_cache(SlabAlloc *cache){
	if(cache->cantidad_en_uso == 0){
		void *ptr = cache->mem_ptr;
		int i = 0;
		for(i = 0; i < cache->tamano_cache;i++){
			cache->destructor(ptr+(i*cache->tamano_objeto),cache->tamano_objeto);
		}
		free(cache->slab_ptr);
		free(cache->mem_ptr);
		free(cache);
	}
}

void stats_cache(SlabAlloc *cache){
	printf("Nombre de cache: %s\n",(cache->nombre));
	printf("Cantidad de slabs: %d\n",cache->tamano_cache);
	printf("Cantidad de slabs en uso: %d\n",cache->cantidad_en_uso);
	printf("Tamanio de objeto: %lu\n\n",cache->tamano_objeto);
	printf("Direccion de cache->mem_ptr: %p\n",cache->mem_ptr);
	int i = 0;
	Slab *ptrslab2 =(cache->slab_ptr);
	for(i = 0;i < cache->tamano_cache;i++){
		if(ptrslab2[i].status == 1)
			printf("direccion ptr[%d].ptr_data: %p  <EN_USO>  ptr[%d].ptr_data_old: %p\n",i,(ptrslab2[i].ptr_data),i,(ptrslab2[i].ptr_data_old));
		else if(ptrslab2[i].status == 0){
			printf("direccion ptr[%d].ptr_data: %p  <DISPONIBLE>  ptr[%d].ptr_data_old: %p\n",i,(ptrslab2[i].ptr_data),i,(ptrslab2[i].ptr_data_old));
		}
	}

}