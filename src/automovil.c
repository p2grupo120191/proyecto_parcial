#include <unistd.h>
#include <stdlib.h>
#include "objetos.h"

void crear_automovil(void *ref, size_t tamano){
	Automovil *buf=(Automovil *)ref;
	buf->marca = (char*)calloc(20,sizeof(char));
	buf->modelo = (char*)calloc(30,sizeof(char));
	buf->precio = 0.0f;
	buf->revision_tec = 0;	

}

void eliminar_automovil(void *ref, size_t tamano){
	Automovil *buf=(Automovil *)ref;
	free(buf->marca);
	free(buf->modelo);
	buf->precio = -1;
	buf->revision_tec = -1;

}
